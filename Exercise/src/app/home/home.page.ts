import { Component } from '@angular/core';
import { MockService } from '../services/mock.service';

declare var cordova: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  data$: any;
  mockData: any;
  constructor(private mock: MockService) {}

  ngOnInit() {
    this.data$ = this.mock.getAssetOverview();
    this.data$.subscribe(data => this.mockData = data);
  }

  buy(){
    console.log("buy");
    var pricePerUnit = this.mockData.summary[0].value
    var quantity = +this.mockData.summary[1].value
    var totalAmount = +this.mockData.summary[2].value

    //TODO - Change implementation to Ionic native and Angular.
    cordova.plugins.AvaloqModal.showModal([pricePerUnit], 
      function(result){
        //Success - Return from narive after trigger of Preview
        console.log("success: " + JSON.stringify(result));
        var exPectedData = result;
        var buyQuantity = +exPectedData[0];
        var buyAmount = +exPectedData[1];
        console.log("quantity: " + quantity + " totalAmount: " + totalAmount + " buyQuantity: " + buyQuantity + " buyAmount: " + buyAmount);
        quantity += buyQuantity;
        totalAmount += buyAmount;
        console.log("quantity: " + quantity + " totalAmount: " + totalAmount);
        alert("quantity + bought: " + quantity + " totalAmount + bought: " + totalAmount);
        //TODO - Display data to UI screen

    }, function(result){
      //Do nothing, already closed the modal in native code.
      console.log("Close: " + JSON.stringify(result));
    });

  }

  ngOnDestroy() {
    this.data$.unsubscribe();
  }

}
