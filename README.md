
# Ionic 4 Exercise

## Getting Started

An Ionic 4 project that will call a native modal plugin.

### Problem 1 : Create an Ionic Angular application
------
1. Use Ionic components as much as possible to implement the design.
2. Create an Angular service that serves the mock data to be displayed in the page. There is no need to follow a specific structure for the data.
3. Create two themes for both, Android and iOS, and feel free to choose the one for Android.
4. For this assessment none of the items should trigger any action.

### Problem 2 : Create a native modal
------
1. The image given can be used as guideline for the modal. Other designs are permitted as long as the following elements are present.
	- Field "QUANTITY"
	- Field "EST. CHF AMOUNT"
	- Close ("X") button
	- "PREVIEW" button
2. The price displayed in the modal (100 in the previous case) should be provided by the Angular service implemented in Problem 1 and sent it to the native layer.
3. For the communication between web and native, you can use Apache Cordova or Ionic Capacitor for Android, or Swift for iOS.
4. Depending on the number specified in the field "QUANTITY", the field "EST. CHF AMOUNT" will be automatically updated by the following this simple mathematical operations:
	- Est. Chf amount = quantity * price
5. The rest of the data displayed in the modal can be hardcoded.
6. For this assessment none of the actionable items should trigger any action, except for the next items:
	- The close button will close the modal without passing any data back to the web view.
	- The preview button will close the modal and return the values "QUANTITY" and "EST. CHF AMOUNT" to the web view.
7. Depending on the values returned, the web view will update accordingly the section "TOTAL UNITS" and "TOTAL AMOUNT".

### Prerequisite
------
- **Cordova**, make sure you have [Node.js](http://nodejs.org/) installed, then run
```
npm install -g cordova
```
- **Ionic**
```
npm install -g ionic
```

### Steps to install
------
- Run the command in the Ionic project root folder.

Install the dependencies:
```
npm install
```
Add platforms, build and test the app:
```
ionic cordova platform add ios
ionic cordova platform add android
ionic cordova build ios
ionic cordova emulate ios
```



## Solution 1:

- Created an Ionic blank project.
- Created an app-header component and used in home page.
- Added JSON for mock data in assets folder, formatted some data in array of objects or array for easier loading.
- Created a service to retrieve the mock data and used Observables.
- Put the theme for the different platforms in theme/variables.scss -  *.md*(material design) for android and *.ios* for iOS
- Initialize the data in ngOnInit to call the service only once since its just a static data.



## Solution 2: Swift + Storyboard via Apache Cordova

Add the plugin to the project, then build and test on iOS device:
```
ionic cordova plugin add https://black0089@bitbucket.org/black0089/cordova-plugin-avaloqmodal.git
```
- Chose to implement swift + storyboard because I think among the 3 choices, it's the most difficult. Most of the cordova plugins are written in objective-c, you can rarely see plugins that are written in swift. 
- Also, if it's written in swift, mostly the UI is written programmatically. In my experience, I haven't seen any cordova plugin yet that is written in swift and uses a storyboard.
- I chose to display the modal via storyboard because if we are going to integrate the iOS native and hybrid app, I'd assume that the iOS native app is using storyboard, that's the reason I chose this approach to display my skills that I can integrate Avaloq iOS native apps in Ionic.
- Created an initial cordova plugin via plugman.
- Deleted the obj-c class and added the necessary files for the modal plugin.
- Modified the plugin.xml to include 2 swift source files, 1 header file, 1 storyboard and added the cordova plugin swift support.
- Passed the price per unit data from web view to iOS native CDVPlugin class and initiated to show the modal which is controlled by another swift class.
- Passed the quantity and Est. CHF Amount and status "CDVCommandStatus_OK" to the web view if the user triggers "Preview" so that the success callback will be called.
- Passed the status "CDVCommandStatus_ERROR" if the user triggers the close button so that the other callback function will be called and the app will just do nothing.

